# themeui

1. Install Java JDK. Set your `JAVA_HOME` environment variable to your JDK folder.
2. Run `./mvnw spring-boot:run`
3. Go to <http://localhost:8080>
