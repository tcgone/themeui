package tcgwars.dev.theme;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import tcgwars.dev.theme.MainWindow;

@SpringUI
@Theme("tcgwars")
@Title("TCG ONE - Theme UI")
public class ThemeUI extends UI {

	protected void init(VaadinRequest request) {
		setContent(new MainWindow());
	}

}
