package tcgwars.dev.theme;

import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinServletService;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.spring.server.SpringVaadinServletService;

public class VaadinServlet extends SpringVaadinServlet {

	@Override
	protected VaadinServletService createServletService(DeploymentConfiguration deploymentConfiguration) throws ServiceException {
		SpringVaadinServletService servletService = new SpringVaadinServletService(this, deploymentConfiguration, getServiceUrlPath());
		servletService.init();
		return servletService;
	}
}
