package tcgwars.dev.theme;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;

public class GameView extends HorizontalLayout {

	private LogPanel logChat;
	private ActionsPanel actionsA;
	private ActionsPanel actionsB;

	private HandPanel handA;
	private HandPanel handB;

	private CustomPbgView oppPbgView;
	private CustomPbgView ownPbgView;

	public void initUI() {
		setSizeFull();

		removeAllComponents();

		VerticalLayout left = new VerticalLayout();
		left.setSpacing(true);
		left.setWidth(250, Unit.PIXELS);
		left.setHeight(100, Unit.PERCENTAGE);
		this.addComponent(left);

		actionsB = new ActionsPanel();
		left.addComponent(actionsB);
		left.setExpandRatio(actionsB, 6);

		DeveloperPanel developerPanel = new DeveloperPanel();
		left.addComponent(developerPanel);

		actionsA = new ActionsPanel();
		left.addComponent(actionsA);
		left.setExpandRatio(actionsA, 6);

		logChat = new LogPanel();
		logChat.setSizeFull();

		VerticalLayout middle = new VerticalLayout();
		middle.setSizeFull();

		HorizontalSplitPanel split = new HorizontalSplitPanel(logChat, middle);
		split.setSplitPosition(20);
		split.setMinSplitPosition(15, Unit.PERCENTAGE);
		split.setMaxSplitPosition(50, Unit.PERCENTAGE);
		this.addComponent(split);
		this.setExpandRatio(split, 1);

		oppPbgView = new CustomPbgView(2);
		ownPbgView = new CustomPbgView(1);
		handA = new HandPanel();
		handB = new HandPanel();

		middle.addComponent(handB);
		middle.addComponent(oppPbgView);
		middle.addComponent(ownPbgView);
		middle.addComponent(handA);
		middle.setExpandRatio(handB, 1);
		middle.setExpandRatio(oppPbgView, 2);
		middle.setExpandRatio(ownPbgView, 2);
		middle.setExpandRatio(handA, 1);
	}

	public void updateUI() {
		actionsA.updateUI();
		actionsB.updateUI();
		handA.updateUI();
		handB.updateUI();
		ownPbgView.updateUI();
		oppPbgView.updateUI();
		logChat.updateUI();
	}

}
