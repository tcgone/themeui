package tcgwars.dev.theme;

import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class CustomPbgView extends CustomLayout {

	private PokemonStatusPanel activeStatus, benchStatus[];
	private CardView active, bench[];
	private PlayerStatusPanel playerStatusPanel;

	static String readTemplate(String name) {
		try {
			InputStream inputStream = CustomPbgView.class.getResourceAsStream(name);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(inputStream, baos);
			baos.close();
			String theString = baos.toString("UTF-8");
			return theString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public CustomPbgView(int type) {
		if (type == 2) {
			setTemplateContents(readTemplate("/layouts/opp_pbg.html"));
		} else if (type == 1) {
			setTemplateContents(readTemplate("/layouts/own_pbg.html"));
		} else {
			throw new IllegalArgumentException();
		}
		setHeight("270px");
		setWidth("100%");

		//player status panel
		playerStatusPanel = new PlayerStatusPanel();
		playerStatusPanel.setWidth("120px");
		playerStatusPanel.setHeight("40px");
		this.addComponent(playerStatusPanel, "player_s");

		//active pokemon and benches
		bench = new CardView[6];
		benchStatus = new PokemonStatusPanel[6];

		activeStatus = new PokemonStatusPanel(type == 2);
		this.addComponent(activeStatus, "active_s");

		active = new CardView(CardImageCache.randomCard());
		active.setWidth("150px");
		active.setHeight("207px");
//		active.setHeight("100%");
		this.addComponent(active, "active_c");

		for (int i = 0; i < benchStatus.length; i++) {

			benchStatus[i] = new PokemonStatusPanel(type == 2);
			this.addComponent(benchStatus[i], "bench" + i + "_s");

			bench[i] = new CardView(CardImageCache.getBlankImage());
			bench[i].setWidth("100px");
			bench[i].setHeight("138px");
//			bench[i].setHeight("100%");
			this.addComponent(bench[i], "bench" + i + "_c");
		}

	}

	public void updateUI() {
		for (int i = 0; i < 6; i++) {
			CardView cw = new CardView(CardImageCache.getBackImage());
			cw.setWidth(50, Unit.PIXELS);
			cw.setHeight(68, Unit.PIXELS);
			this.addComponent(cw, "prize" + i);
		}
		activeStatus.updateUI();

		int benchSize = (int) (2 + Math.random() * 5);
		for (int i = 0; i < benchSize; i++) {
			benchStatus[i].updateUI();
			bench[i].setSource(CardImageCache.randomCard());
		}

		this.addComponent(new Label(String.format("Deck[%d]", 50)), "deck_l");
		CardView deck = new CardView(CardImageCache.getBackImage());
		deck.setWidth("60px");
		deck.setHeight("83px");
		this.addComponent(deck, "deck_c");

		this.addComponent(new Label(String.format("Discard[%d]", 3)), "discard_l");
		CardView discard = new CardView(CardImageCache.randomCard());
		discard.setWidth("60px");
		discard.setHeight("83px");
		this.addComponent(discard, "discard_c");

		this.addComponent(new Label(String.format("Hand[%d]", 7)), "hand_l");

	}

}
