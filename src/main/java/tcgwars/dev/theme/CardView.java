package tcgwars.dev.theme;

import com.vaadin.server.Resource;
import com.vaadin.ui.Image;

public class CardView extends Image {

	public static final String NORMAL = "cv";
	public static final String SELECTED = "cvselected";
	public static final String HIDDEN_STYLE = "cvhidden";
	public static final String BLANK_STYLE = "cvblank";

	public CardView(Resource card) {
		this.setStyleName(NORMAL);
		this.setSource(card);
	}

	public enum CardViewMode {
		HIDDEN, BLANK, NORMAL
	}
}
