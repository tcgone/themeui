package tcgwars.dev.theme;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;

public class CardImageCache {

	private static Resource backImage = new ThemeResource("card_images/back.jpg");
	private static Resource blankImage = new ThemeResource("card_images/blank.png");

	public static Resource randomCard(){
		return new ExternalResource(String.format("https://tcgone.net/scans/l/skyridge/%03d.jpg", (int) (Math.random() * 100)));
	}

	public static Resource getBackImage() {
		return backImage;
	}
	public static Resource getBlankImage() {
		return blankImage;
	}

}
