package tcgwars.dev.theme;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ActionsPanel extends Panel {

	private VerticalLayout layout = new VerticalLayout();

	public ActionsPanel() {
		layout.setMargin(true);
		layout.setSpacing(true);
		setContent(layout);
		setWidth(100, Unit.PERCENTAGE);
	}
	
	public void updateUI(){
		layout.removeAllComponents();
		layout.addComponent(getButton("Action 1"));
		layout.addComponent(getButton("Action 2"));
		layout.addComponent(getButton("Action 3"));
		layout.addComponent(getButton("Action 4"));
		layout.addComponent(getButton("Retreat"));
		layout.addComponent(getButton("End Turn"));
	}
	
	private Button getButton(String title){
		Button b = new Button(title);
		b.setWidth(100, Unit.PERCENTAGE);
		b.setDisableOnClick(true);
		b.setStyleName(ValoTheme.BUTTON_PRIMARY);
		return b;
	}

}
