package tcgwars.dev.theme;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

public class MainWindow extends VerticalLayout {

	public MainWindow() {
		this.setSizeFull();

		HorizontalLayout navLayout = new HorizontalLayout();
		navLayout.setWidth("100%");
		this.addComponent(navLayout);

		Button newGameButton = new Button("New Game");
		Button spacer = new Button("TCG ONE Theme UI");
		spacer.setStyleName("link2 link");

		MenuBar userMenuBar = new MenuBar();
		MenuBar.MenuItem userMenu = userMenuBar.addItem("UserMenu", FontAwesome.USER, null);
		userMenu.addItem("Logout", FontAwesome.SIGN_OUT, selectedItem -> {});

		navLayout.addComponent(newGameButton);
		navLayout.setComponentAlignment(newGameButton, Alignment.MIDDLE_LEFT);
		navLayout.addComponent(spacer);
		navLayout.setComponentAlignment(spacer, Alignment.MIDDLE_CENTER);
		navLayout.setExpandRatio(spacer, 1);
		navLayout.addComponent(userMenuBar);
		navLayout.setComponentAlignment(userMenuBar, Alignment.MIDDLE_RIGHT);

		GameView gameView = new GameView();
		this.addComponent(gameView);
		this.setExpandRatio(gameView, 1);

		gameView.initUI();
		gameView.updateUI();
	}
	
}
