package tcgwars.dev.theme;

import com.vaadin.server.Resource;
import com.vaadin.ui.CssLayout;

import java.util.ArrayList;
import java.util.List;

public class HandPanel extends CssLayout {

	public HandPanel() {
		setHeight("105px");
		setWidth(100, Unit.PERCENTAGE);
		addStyleName("hand");
	}

	public void updateUI() {
		List<Resource> hand = new ArrayList<>();
		for (int i = 0; i < 7; i++) {
			hand.add(CardImageCache.randomCard());
		}
		this.removeAllComponents();

		int width = 72;
		if (hand.size() > 15) {
			width = 40;
		} else if (hand.size() > 10) {
			width = 53;
		}

		for (final Resource card : hand) {
			CardView cw = new CardView(card);
			cw.setWidth(width, Unit.PIXELS);
			addComponent(cw);
		}

	}
}
