package tcgwars.dev.theme;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class DeveloperPanel extends Panel {
	public DeveloperPanel() {
		setWidth(100, Unit.PERCENTAGE);
		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		layout.setSpacing(true);
		setContent(layout);

		layout.addComponents(new Button("Do Nothing"));
	}

}
