package tcgwars.dev.theme;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class LogPanel extends VerticalLayout {

	private Panel panel;
	private CssLayout panelLayout;

	public LogPanel() {
		setSpacing(true);

		panel = new Panel();
		panelLayout = new CssLayout();
		panelLayout.setStyleName("chatlog");
		panel.setContent(panelLayout);
		addComponent(panel);
		panel.setSizeFull();
		setExpandRatio(panel, 1);
	}

	public void updateUI() {
		panelLayout.removeAllComponents();
		for (int i = 0; i < 60; i++) {
			String s = "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium "+i;
			panelLayout.addComponent(new Label("● " + s, ContentMode.HTML));
		}
		panel.setScrollTop(Short.MAX_VALUE);
	}

}
