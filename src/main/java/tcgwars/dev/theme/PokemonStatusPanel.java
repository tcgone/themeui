package tcgwars.dev.theme;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class PokemonStatusPanel extends VerticalLayout {

	private boolean statusOnTop;

	public PokemonStatusPanel(boolean statusOnTop){
//		setWidth("100px");
		setHeight("40px");
		this.statusOnTop = statusOnTop;
	}

	public void updateUI(){
		this.removeAllComponents();

		CssLayout cond = new CssLayout();
		//trainer count
		{
			Label l = new Label(1+"");
			l.setIcon(FontAwesome.WRENCH);
			l.setStyleName("pcssi");
			l.setDescription("Trainers: [Pluspower]");
			cond.addComponent(l);
		}
		//sp.energy count
		{
			Label l = new Label(2+"");
			l.setIcon(FontAwesome.ASTERISK);
			l.setStyleName("pcssi");
			l.setDescription("Special Energy: [Double Rainbow Energy, Metal Energy]");
			cond.addComponent(l);
		}
		//damage
		Label dmglbl = new Label(10+"");
		dmglbl.addStyleName("dmgLbl");
		dmglbl.setHeight("20px");
		dmglbl.setDescription("Damage: "+10);
		cond.addComponent(dmglbl);

		CssLayout energy = new CssLayout();
		for (int i=0; i<2; i++) {
			Embedded img = new Embedded();
			img.setWidth("22px");
			img.setHeight("20px");
			img.setSource(new ThemeResource("types/water.png"));
			img.setStyleName("tyi");
			energy.addComponent(img);
		}
		if(statusOnTop){
			this.addComponent(cond);
			this.addComponent(energy);
		} else {
			this.addComponent(energy);
			this.addComponent(cond);
		}
	}
}
